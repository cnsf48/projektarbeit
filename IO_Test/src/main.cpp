#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <wiringPiSPI.h>
#include <pigpio.h>
#include "print.hpp"
#include "lcd.hpp"

#define StringLenght 100
#define row_lenght 16
#define rows 3
#define speed 32000000
#define i2c_adress 0x3c
#define spi_channel 0
	


void spi_send_data(unsigned char* buffer, size_t size){
	unsigned char* internal_buffer = (unsigned char*) malloc(size);
	memcpy (internal_buffer, buffer, size);
	wiringPiSPIDataRW(spi_channel, internal_buffer, size) ;
	free (internal_buffer);
}

unsigned char get_keys(int device){
	static unsigned char last_value = 0;
	unsigned char value = wiringPiI2CReadReg8 (device, 0x05);
	unsigned char return_value = last_value & ~value;
	//if(return_value || value & 2 || last_value & 2){
	//	printf("Keys: 0x%02x\n", return_value);
	//	printf("Keys now: 0x%02x\n", value);
	//	printf("Keys last: 0x%02x\n", last_value);		
	//}
	last_value = value;
	return return_value;
}

#define bits 16
#define base_frequency 133000000
double set_frequency(int device, double frequency){
	unsigned char group = 0;
	unsigned int count = 0;
	double new_base_frequency = base_frequency;
	double min_group_frequency = base_frequency;
	int x = 0;	
	while(min_group_frequency > frequency){
		min_group_frequency = new_base_frequency / (pow(2, bits)-1);
		x++;
		new_base_frequency = base_frequency / pow(2, x);
	}
	x--;
	if(x<0){
		printf("frequency out of range\n");
		return -1;
	}
	group = x;
	new_base_frequency = base_frequency / pow(2, x);
	min_group_frequency = new_base_frequency / (pow(2, bits));
	double last_value = min_group_frequency;
	double last_difference = abs(frequency - last_value);
	double value;
	double current_difference;
	for( x = pow(2, bits) ; x > 0; x--){
		value = new_base_frequency / x;
		current_difference = abs(frequency - value);
		if(last_difference < current_difference) 
			break;
		last_difference = current_difference;
	}
	count = x + 1;
	wiringPiI2CWriteReg8 (device, 0x02, group);
	wiringPiI2CWriteReg8 (device, 0x03, (unsigned char)(x & 0xFF));
	wiringPiI2CWriteReg8 (device, 0x01, (unsigned char)((x >> 8) & 0xFF));
	return (base_frequency / pow(2, group)) / count;
}

int main(void){
	wiringPiSetupSys();
	if (gpioInitialise() < 0){
		printf("Error while Initialise pigpio\n");
		return 1;
	} 

	int fpga_i2c;
	fpga_i2c = wiringPiI2CSetup(i2c_adress);
	lcd_device_nr = fpga_i2c;
	int fpga_spi;
	fpga_spi = wiringPiSPISetup (spi_channel, speed) ;

	char mystring [StringLenght];

	if(fpga_i2c == -1){
		printf("No I2C device found\n");
		return 0;
	}
	if(fpga_spi == -1){
		printf("Cant open SPI device\n");
		return 0;
	}

	lcd_init();

	//printf("Display: ");
	//fgets (mystring, StringLenght, stdin);
	//
	//lcd_set_cursor(fpga_i2c, 0, 1);ltra 
	//lcd_puts(fpga_i2c, mystring);

	//int x = 0;
	//int y = 0;
	//char text [17];
	//char fifo_state;
	//unsigned char send_data [] {0x39,0x15,0x55,0x6E,0x70,0x38,0x0C,0x01,0x00,0xFF};
	//unsigned char send_data [] {0x15,0x11,0x22,0x44,0x88,0x55,0x55,0x55,0x55,0x55};
	//unsigned char send_data [] {0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	unsigned char keys = 0;	
	int options = 0;

	while (true)
	{
		keys = get_keys(fpga_i2c);
		if(keys & 2){
			lcd_set_cursor(0,0);
			printf("Option: ");
			lcd_printf("Option: ");
			fgets (mystring, StringLenght, stdin);
			sscanf(mystring, "%d", &options);
			double frequency = 0;					
			switch(options){
				case 1:
					printf("Display: ");
					lcd_set_cursor(0,1);
					lcd_printf("Display: ");
					fgets (mystring, StringLenght, stdin);
					lcd_set_cursor(0, 2);
					lcd_printf("%s ",mystring);
					break;
				case 2:
					printf("Frequency: ");
					lcd_set_cursor(0,1);
					lcd_printf("Frequency: ");
					fgets (mystring, StringLenght, stdin);
					sscanf(mystring, "%lf", &frequency);
					printf("Frequency-out: %lf\n", set_frequency(fpga_i2c, frequency));	
					lcd_set_cursor(0,2);
					lcd_printf("Frequency-out: %lf\n", set_frequency(fpga_i2c, frequency));					
					break;
				case 3:
					printf("Frequency2: ");
					lcd_set_cursor(0,1);
					lcd_printf("Frequency2: ");
					fgets (mystring, StringLenght, stdin);
					sscanf(mystring, "%lf", &frequency);
					printf("Return: %i\n", gpioHardwareClock(4,(unsigned int)frequency));
					lcd_set_cursor(0,2);	
					lcd_printf("Return: %i\n", gpioHardwareClock(4,(unsigned int)frequency));				
					break;					
			}

		}
		//x = wiringPiI2CReadReg8 (fpga_i2c, 0x04) ;
		//if(x != y){
		//	printf("\r%i   ", x);
		//	fflush(stdout);
		//	sprintf(text,"%i",x);
		//	lcd_set_cursor(fpga_i2c, 0, 2);
		//	lcd_puts(fpga_i2c, text);
		//	lcd_puts(fpga_i2c, "     ");
		//	//wiringPiI2CWriteReg8 (fpga_i2c, 0x03, x);
		//}
		//y = x;
		//fifo_state = wiringPiI2CReadReg8 (fpga_i2c, 0x06) & 0x0F;
		//lcd_set_cursor(fpga_i2c, 0, 0);
		//switch(fifo_state){
		//	case 0x03:
		//		lcd_puts(fpga_i2c, "Empty       ");
		//		break;
		//	case 0x02:
		//		lcd_puts(fpga_i2c, "Almost Empty");
		//		break;
		//	case 0x04:
		//		lcd_puts(fpga_i2c, "Almost Full ");
		//		break;
		//	case 0x0c:
		//		lcd_puts(fpga_i2c, "Full        ");
		//		break;
		//	default:
		//		lcd_puts(fpga_i2c, "Reset State ");
		//		break;					
		//}
		//sprintf(text, "0x%02x", fifo_state);
		//lcd_puts(fpga_i2c, text);
		//delay(1);
		//if(!(fifo_state & 0x0c))
		//	spi_send_data(send_data, 10);
	}
	return 0;
}