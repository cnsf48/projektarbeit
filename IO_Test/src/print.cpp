#include "print.hpp"
#include "lcd.hpp"

char buffer[80];
int lcd_printf(const char *format, ...) {
	va_list aptr;
	int ret;

	va_start(aptr, format);
	ret = vsprintf(buffer, format, aptr);
	va_end(aptr);

	lcd_puts(buffer);

	return(ret);
}
