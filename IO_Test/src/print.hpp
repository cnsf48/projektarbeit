#ifndef __print_h__
#define __print_h__

#include <stdio.h>
#include <stdarg.h>

int lcd_printf(const char *format, ...);

#endif // __print_h__