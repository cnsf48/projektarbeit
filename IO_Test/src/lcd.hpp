#ifndef __lcd_h__
#define __lcd_h__

#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <unistd.h>

#define row_lenght 16
#define rows 3

extern int lcd_device_nr;

void lcd_init();
void lcd_init(int device);
void lcd_puts(char* text);
void lcd_puts(int device, char* text);
void lcd_put(char c);
void lcd_put(int device, char c);
void lcd_set_cursor(unsigned char x, unsigned char y);
void lcd_set_cursor(int device, unsigned char x, unsigned char y);

#endif // __lcd_h__