﻿namespace raw_transmissiondata_converter
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_code = new System.Windows.Forms.GroupBox();
            this.rb_uart_lsb = new System.Windows.Forms.RadioButton();
            this.rb_hex_output = new System.Windows.Forms.RadioButton();
            this.rb_biphase_mark = new System.Windows.Forms.RadioButton();
            this.rb_ohne = new System.Windows.Forms.RadioButton();
            this.rb_miller = new System.Windows.Forms.RadioButton();
            this.rb_manchester = new System.Windows.Forms.RadioButton();
            this.rb_huffman = new System.Windows.Forms.RadioButton();
            this.rb_8b10b = new System.Windows.Forms.RadioButton();
            this.rb_5b6b = new System.Windows.Forms.RadioButton();
            this.rb_3b4b = new System.Windows.Forms.RadioButton();
            this.rb_morse = new System.Windows.Forms.RadioButton();
            this.rb_spdif = new System.Windows.Forms.RadioButton();
            this.rb_uart = new System.Windows.Forms.RadioButton();
            this.btn_open = new System.Windows.Forms.Button();
            this.txt_input_file_path = new System.Windows.Forms.TextBox();
            this.group_input = new System.Windows.Forms.GroupBox();
            this.rb_hex = new System.Windows.Forms.RadioButton();
            this.rb_texteingabe = new System.Windows.Forms.RadioButton();
            this.rb_wav = new System.Windows.Forms.RadioButton();
            this.rb_ascii = new System.Windows.Forms.RadioButton();
            this.rb_raw = new System.Windows.Forms.RadioButton();
            this.txt_eingabe = new System.Windows.Forms.TextBox();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.txt_output_file_path = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_run = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_open_run = new System.Windows.Forms.Button();
            this.btn_save_run = new System.Windows.Forms.Button();
            this.group_save = new System.Windows.Forms.GroupBox();
            this.num_samplerate = new System.Windows.Forms.NumericUpDown();
            this.rb_sr_8bit = new System.Windows.Forms.RadioButton();
            this.rb_sr_digi = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.group_open = new System.Windows.Forms.GroupBox();
            this.rb_open_text = new System.Windows.Forms.RadioButton();
            this.rb_open_raw = new System.Windows.Forms.RadioButton();
            this.group_code.SuspendLayout();
            this.group_input.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.group_save.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_samplerate)).BeginInit();
            this.group_open.SuspendLayout();
            this.SuspendLayout();
            // 
            // group_code
            // 
            this.group_code.Controls.Add(this.rb_uart_lsb);
            this.group_code.Controls.Add(this.rb_hex_output);
            this.group_code.Controls.Add(this.rb_biphase_mark);
            this.group_code.Controls.Add(this.rb_ohne);
            this.group_code.Controls.Add(this.rb_miller);
            this.group_code.Controls.Add(this.rb_manchester);
            this.group_code.Controls.Add(this.rb_huffman);
            this.group_code.Controls.Add(this.rb_8b10b);
            this.group_code.Controls.Add(this.rb_5b6b);
            this.group_code.Controls.Add(this.rb_3b4b);
            this.group_code.Controls.Add(this.rb_morse);
            this.group_code.Controls.Add(this.rb_spdif);
            this.group_code.Controls.Add(this.rb_uart);
            this.group_code.Location = new System.Drawing.Point(12, 12);
            this.group_code.Name = "group_code";
            this.group_code.Size = new System.Drawing.Size(124, 297);
            this.group_code.TabIndex = 0;
            this.group_code.TabStop = false;
            this.group_code.Text = "Leitungscode";
            // 
            // rb_uart_lsb
            // 
            this.rb_uart_lsb.AutoSize = true;
            this.rb_uart_lsb.Location = new System.Drawing.Point(67, 19);
            this.rb_uart_lsb.Name = "rb_uart_lsb";
            this.rb_uart_lsb.Size = new System.Drawing.Size(45, 17);
            this.rb_uart_lsb.TabIndex = 13;
            this.rb_uart_lsb.Text = "LSB";
            this.rb_uart_lsb.UseVisualStyleBackColor = true;
            // 
            // rb_hex_output
            // 
            this.rb_hex_output.AutoSize = true;
            this.rb_hex_output.Location = new System.Drawing.Point(6, 272);
            this.rb_hex_output.Name = "rb_hex_output";
            this.rb_hex_output.Size = new System.Drawing.Size(47, 17);
            this.rb_hex_output.TabIndex = 12;
            this.rb_hex_output.Text = "HEX";
            this.rb_hex_output.UseVisualStyleBackColor = true;
            // 
            // rb_biphase_mark
            // 
            this.rb_biphase_mark.AutoSize = true;
            this.rb_biphase_mark.Location = new System.Drawing.Point(6, 249);
            this.rb_biphase_mark.Name = "rb_biphase_mark";
            this.rb_biphase_mark.Size = new System.Drawing.Size(90, 17);
            this.rb_biphase_mark.TabIndex = 10;
            this.rb_biphase_mark.Text = "Biphase Mark";
            this.rb_biphase_mark.UseVisualStyleBackColor = true;
            // 
            // rb_ohne
            // 
            this.rb_ohne.AutoSize = true;
            this.rb_ohne.Location = new System.Drawing.Point(6, 226);
            this.rb_ohne.Name = "rb_ohne";
            this.rb_ohne.Size = new System.Drawing.Size(75, 17);
            this.rb_ohne.TabIndex = 9;
            this.rb_ohne.Text = "ohne (SPI)";
            this.rb_ohne.UseVisualStyleBackColor = true;
            // 
            // rb_miller
            // 
            this.rb_miller.AutoSize = true;
            this.rb_miller.Location = new System.Drawing.Point(6, 203);
            this.rb_miller.Name = "rb_miller";
            this.rb_miller.Size = new System.Drawing.Size(49, 17);
            this.rb_miller.TabIndex = 8;
            this.rb_miller.Text = "Miller";
            this.rb_miller.UseVisualStyleBackColor = true;
            // 
            // rb_manchester
            // 
            this.rb_manchester.AutoSize = true;
            this.rb_manchester.Location = new System.Drawing.Point(6, 180);
            this.rb_manchester.Name = "rb_manchester";
            this.rb_manchester.Size = new System.Drawing.Size(81, 17);
            this.rb_manchester.TabIndex = 7;
            this.rb_manchester.Text = "Manchester";
            this.rb_manchester.UseVisualStyleBackColor = true;
            // 
            // rb_huffman
            // 
            this.rb_huffman.AutoSize = true;
            this.rb_huffman.Location = new System.Drawing.Point(6, 157);
            this.rb_huffman.Name = "rb_huffman";
            this.rb_huffman.Size = new System.Drawing.Size(65, 17);
            this.rb_huffman.TabIndex = 6;
            this.rb_huffman.Text = "Huffman";
            this.rb_huffman.UseVisualStyleBackColor = true;
            // 
            // rb_8b10b
            // 
            this.rb_8b10b.AutoSize = true;
            this.rb_8b10b.Location = new System.Drawing.Point(6, 134);
            this.rb_8b10b.Name = "rb_8b10b";
            this.rb_8b10b.Size = new System.Drawing.Size(55, 17);
            this.rb_8b10b.TabIndex = 5;
            this.rb_8b10b.Text = "8b10b";
            this.rb_8b10b.UseVisualStyleBackColor = true;
            // 
            // rb_5b6b
            // 
            this.rb_5b6b.AutoSize = true;
            this.rb_5b6b.Location = new System.Drawing.Point(6, 111);
            this.rb_5b6b.Name = "rb_5b6b";
            this.rb_5b6b.Size = new System.Drawing.Size(49, 17);
            this.rb_5b6b.TabIndex = 4;
            this.rb_5b6b.Text = "5b6b";
            this.rb_5b6b.UseVisualStyleBackColor = true;
            // 
            // rb_3b4b
            // 
            this.rb_3b4b.AutoSize = true;
            this.rb_3b4b.Location = new System.Drawing.Point(6, 88);
            this.rb_3b4b.Name = "rb_3b4b";
            this.rb_3b4b.Size = new System.Drawing.Size(49, 17);
            this.rb_3b4b.TabIndex = 3;
            this.rb_3b4b.Text = "3b4b";
            this.rb_3b4b.UseVisualStyleBackColor = true;
            // 
            // rb_morse
            // 
            this.rb_morse.AutoSize = true;
            this.rb_morse.Location = new System.Drawing.Point(6, 65);
            this.rb_morse.Name = "rb_morse";
            this.rb_morse.Size = new System.Drawing.Size(54, 17);
            this.rb_morse.TabIndex = 2;
            this.rb_morse.Text = "Morse";
            this.rb_morse.UseVisualStyleBackColor = true;
            // 
            // rb_spdif
            // 
            this.rb_spdif.AutoSize = true;
            this.rb_spdif.Location = new System.Drawing.Point(6, 42);
            this.rb_spdif.Name = "rb_spdif";
            this.rb_spdif.Size = new System.Drawing.Size(56, 17);
            this.rb_spdif.TabIndex = 1;
            this.rb_spdif.Text = "SPDIF";
            this.rb_spdif.UseVisualStyleBackColor = true;
            // 
            // rb_uart
            // 
            this.rb_uart.AutoSize = true;
            this.rb_uart.Checked = true;
            this.rb_uart.Location = new System.Drawing.Point(6, 19);
            this.rb_uart.Name = "rb_uart";
            this.rb_uart.Size = new System.Drawing.Size(55, 17);
            this.rb_uart.TabIndex = 0;
            this.rb_uart.TabStop = true;
            this.rb_uart.Text = "UART";
            this.rb_uart.UseVisualStyleBackColor = true;
            // 
            // btn_open
            // 
            this.btn_open.Location = new System.Drawing.Point(142, 12);
            this.btn_open.Name = "btn_open";
            this.btn_open.Size = new System.Drawing.Size(75, 23);
            this.btn_open.TabIndex = 1;
            this.btn_open.Text = "Öffnen";
            this.btn_open.UseVisualStyleBackColor = true;
            this.btn_open.Click += new System.EventHandler(this.btn_open_Click);
            // 
            // txt_input_file_path
            // 
            this.txt_input_file_path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_input_file_path.Location = new System.Drawing.Point(223, 14);
            this.txt_input_file_path.Name = "txt_input_file_path";
            this.txt_input_file_path.Size = new System.Drawing.Size(584, 20);
            this.txt_input_file_path.TabIndex = 2;
            this.txt_input_file_path.TextChanged += new System.EventHandler(this.txt_input_file_path_TextChanged);
            // 
            // group_input
            // 
            this.group_input.Controls.Add(this.rb_hex);
            this.group_input.Controls.Add(this.rb_texteingabe);
            this.group_input.Controls.Add(this.rb_wav);
            this.group_input.Controls.Add(this.rb_ascii);
            this.group_input.Controls.Add(this.rb_raw);
            this.group_input.Location = new System.Drawing.Point(12, 315);
            this.group_input.Name = "group_input";
            this.group_input.Size = new System.Drawing.Size(124, 139);
            this.group_input.TabIndex = 3;
            this.group_input.TabStop = false;
            this.group_input.Text = "Eingabe";
            // 
            // rb_hex
            // 
            this.rb_hex.AutoSize = true;
            this.rb_hex.Location = new System.Drawing.Point(6, 111);
            this.rb_hex.Name = "rb_hex";
            this.rb_hex.Size = new System.Drawing.Size(47, 17);
            this.rb_hex.TabIndex = 16;
            this.rb_hex.Text = "HEX";
            this.rb_hex.UseVisualStyleBackColor = true;
            // 
            // rb_texteingabe
            // 
            this.rb_texteingabe.AutoSize = true;
            this.rb_texteingabe.Checked = true;
            this.rb_texteingabe.Location = new System.Drawing.Point(6, 88);
            this.rb_texteingabe.Name = "rb_texteingabe";
            this.rb_texteingabe.Size = new System.Drawing.Size(84, 17);
            this.rb_texteingabe.TabIndex = 15;
            this.rb_texteingabe.TabStop = true;
            this.rb_texteingabe.Text = "Texteingabe";
            this.rb_texteingabe.UseVisualStyleBackColor = true;
            // 
            // rb_wav
            // 
            this.rb_wav.AutoSize = true;
            this.rb_wav.Location = new System.Drawing.Point(6, 65);
            this.rb_wav.Name = "rb_wav";
            this.rb_wav.Size = new System.Drawing.Size(50, 17);
            this.rb_wav.TabIndex = 14;
            this.rb_wav.Text = "WAV";
            this.rb_wav.UseVisualStyleBackColor = true;
            // 
            // rb_ascii
            // 
            this.rb_ascii.AutoSize = true;
            this.rb_ascii.Location = new System.Drawing.Point(6, 42);
            this.rb_ascii.Name = "rb_ascii";
            this.rb_ascii.Size = new System.Drawing.Size(52, 17);
            this.rb_ascii.TabIndex = 13;
            this.rb_ascii.Text = "ASCII";
            this.rb_ascii.UseVisualStyleBackColor = true;
            // 
            // rb_raw
            // 
            this.rb_raw.AutoSize = true;
            this.rb_raw.Location = new System.Drawing.Point(6, 19);
            this.rb_raw.Name = "rb_raw";
            this.rb_raw.Size = new System.Drawing.Size(51, 17);
            this.rb_raw.TabIndex = 12;
            this.rb_raw.Text = "RAW";
            this.rb_raw.UseVisualStyleBackColor = true;
            // 
            // txt_eingabe
            // 
            this.txt_eingabe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_eingabe.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_eingabe.Location = new System.Drawing.Point(3, 3);
            this.txt_eingabe.Multiline = true;
            this.txt_eingabe.Name = "txt_eingabe";
            this.txt_eingabe.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_eingabe.Size = new System.Drawing.Size(387, 626);
            this.txt_eingabe.TabIndex = 4;
            this.txt_eingabe.Text = "Hallo Welt!";
            // 
            // txt_output
            // 
            this.txt_output.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_output.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_output.Location = new System.Drawing.Point(3, 3);
            this.txt_output.Multiline = true;
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_output.Size = new System.Drawing.Size(343, 626);
            this.txt_output.TabIndex = 5;
            // 
            // txt_output_file_path
            // 
            this.txt_output_file_path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_output_file_path.Location = new System.Drawing.Point(223, 40);
            this.txt_output_file_path.Name = "txt_output_file_path";
            this.txt_output_file_path.Size = new System.Drawing.Size(584, 20);
            this.txt_output_file_path.TabIndex = 7;
            this.txt_output_file_path.TextChanged += new System.EventHandler(this.txt_output_file_path_TextChanged);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(142, 38);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 6;
            this.btn_save.Text = "Speichern";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_run
            // 
            this.btn_run.Location = new System.Drawing.Point(12, 673);
            this.btn_run.Name = "btn_run";
            this.btn_run.Size = new System.Drawing.Size(124, 23);
            this.btn_run.TabIndex = 8;
            this.btn_run.Text = "Run";
            this.btn_run.UseVisualStyleBackColor = true;
            this.btn_run.Click += new System.EventHandler(this.btn_run_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(142, 67);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txt_eingabe);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txt_output);
            this.splitContainer1.Size = new System.Drawing.Size(746, 632);
            this.splitContainer1.SplitterDistance = 393;
            this.splitContainer1.TabIndex = 9;
            // 
            // btn_open_run
            // 
            this.btn_open_run.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_open_run.Enabled = false;
            this.btn_open_run.Location = new System.Drawing.Point(813, 12);
            this.btn_open_run.Name = "btn_open_run";
            this.btn_open_run.Size = new System.Drawing.Size(75, 23);
            this.btn_open_run.TabIndex = 10;
            this.btn_open_run.Text = "Öffnen";
            this.btn_open_run.UseVisualStyleBackColor = true;
            this.btn_open_run.Click += new System.EventHandler(this.btn_open_run_Click);
            // 
            // btn_save_run
            // 
            this.btn_save_run.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_save_run.Enabled = false;
            this.btn_save_run.Location = new System.Drawing.Point(813, 38);
            this.btn_save_run.Name = "btn_save_run";
            this.btn_save_run.Size = new System.Drawing.Size(75, 23);
            this.btn_save_run.TabIndex = 11;
            this.btn_save_run.Text = "Speichern";
            this.btn_save_run.UseVisualStyleBackColor = true;
            this.btn_save_run.Click += new System.EventHandler(this.btn_save_run_Click);
            // 
            // group_save
            // 
            this.group_save.Controls.Add(this.num_samplerate);
            this.group_save.Controls.Add(this.rb_sr_8bit);
            this.group_save.Controls.Add(this.rb_sr_digi);
            this.group_save.Controls.Add(this.radioButton2);
            this.group_save.Controls.Add(this.radioButton5);
            this.group_save.Location = new System.Drawing.Point(12, 533);
            this.group_save.Name = "group_save";
            this.group_save.Size = new System.Drawing.Size(124, 134);
            this.group_save.TabIndex = 17;
            this.group_save.TabStop = false;
            this.group_save.Text = "Speichern";
            // 
            // num_samplerate
            // 
            this.num_samplerate.Location = new System.Drawing.Point(6, 108);
            this.num_samplerate.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.num_samplerate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_samplerate.Name = "num_samplerate";
            this.num_samplerate.Size = new System.Drawing.Size(112, 20);
            this.num_samplerate.TabIndex = 18;
            this.num_samplerate.Value = new decimal(new int[] {
            50000000,
            0,
            0,
            0});
            // 
            // rb_sr_8bit
            // 
            this.rb_sr_8bit.AutoSize = true;
            this.rb_sr_8bit.Cursor = System.Windows.Forms.Cursors.Default;
            this.rb_sr_8bit.Location = new System.Drawing.Point(6, 88);
            this.rb_sr_8bit.Name = "rb_sr_8bit";
            this.rb_sr_8bit.Size = new System.Drawing.Size(79, 17);
            this.rb_sr_8bit.TabIndex = 17;
            this.rb_sr_8bit.Text = "Sigrok 8 Bit";
            this.rb_sr_8bit.UseVisualStyleBackColor = true;
            // 
            // rb_sr_digi
            // 
            this.rb_sr_digi.AutoSize = true;
            this.rb_sr_digi.Cursor = System.Windows.Forms.Cursors.Default;
            this.rb_sr_digi.Location = new System.Drawing.Point(6, 65);
            this.rb_sr_digi.Name = "rb_sr_digi";
            this.rb_sr_digi.Size = new System.Drawing.Size(87, 17);
            this.rb_sr_digi.TabIndex = 16;
            this.rb_sr_digi.Text = "Sigrok Digital";
            this.rb_sr_digi.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(46, 17);
            this.radioButton2.TabIndex = 15;
            this.radioButton2.Text = "Text";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(6, 19);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(51, 17);
            this.radioButton5.TabIndex = 12;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "RAW";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // group_open
            // 
            this.group_open.Controls.Add(this.rb_open_text);
            this.group_open.Controls.Add(this.rb_open_raw);
            this.group_open.Location = new System.Drawing.Point(12, 460);
            this.group_open.Name = "group_open";
            this.group_open.Size = new System.Drawing.Size(124, 67);
            this.group_open.TabIndex = 17;
            this.group_open.TabStop = false;
            this.group_open.Text = "Öffnen";
            // 
            // rb_open_text
            // 
            this.rb_open_text.AutoSize = true;
            this.rb_open_text.Location = new System.Drawing.Point(6, 42);
            this.rb_open_text.Name = "rb_open_text";
            this.rb_open_text.Size = new System.Drawing.Size(46, 17);
            this.rb_open_text.TabIndex = 13;
            this.rb_open_text.Text = "Text";
            this.rb_open_text.UseVisualStyleBackColor = true;
            // 
            // rb_open_raw
            // 
            this.rb_open_raw.AutoSize = true;
            this.rb_open_raw.Checked = true;
            this.rb_open_raw.Location = new System.Drawing.Point(6, 19);
            this.rb_open_raw.Name = "rb_open_raw";
            this.rb_open_raw.Size = new System.Drawing.Size(51, 17);
            this.rb_open_raw.TabIndex = 12;
            this.rb_open_raw.TabStop = true;
            this.rb_open_raw.Text = "RAW";
            this.rb_open_raw.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 711);
            this.Controls.Add(this.group_open);
            this.Controls.Add(this.group_save);
            this.Controls.Add(this.btn_save_run);
            this.Controls.Add(this.btn_open_run);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btn_run);
            this.Controls.Add(this.txt_output_file_path);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.group_input);
            this.Controls.Add(this.txt_input_file_path);
            this.Controls.Add(this.btn_open);
            this.Controls.Add(this.group_code);
            this.Name = "Form1";
            this.Text = "raw transmissiondata converter";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.group_code.ResumeLayout(false);
            this.group_code.PerformLayout();
            this.group_input.ResumeLayout(false);
            this.group_input.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.group_save.ResumeLayout(false);
            this.group_save.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_samplerate)).EndInit();
            this.group_open.ResumeLayout(false);
            this.group_open.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox group_code;
        private System.Windows.Forms.RadioButton rb_biphase_mark;
        private System.Windows.Forms.RadioButton rb_ohne;
        private System.Windows.Forms.RadioButton rb_miller;
        private System.Windows.Forms.RadioButton rb_manchester;
        private System.Windows.Forms.RadioButton rb_huffman;
        private System.Windows.Forms.RadioButton rb_8b10b;
        private System.Windows.Forms.RadioButton rb_5b6b;
        private System.Windows.Forms.RadioButton rb_3b4b;
        private System.Windows.Forms.RadioButton rb_morse;
        private System.Windows.Forms.RadioButton rb_spdif;
        private System.Windows.Forms.RadioButton rb_uart;
        private System.Windows.Forms.Button btn_open;
        private System.Windows.Forms.TextBox txt_input_file_path;
        private System.Windows.Forms.GroupBox group_input;
        private System.Windows.Forms.RadioButton rb_texteingabe;
        private System.Windows.Forms.RadioButton rb_wav;
        private System.Windows.Forms.RadioButton rb_ascii;
        private System.Windows.Forms.RadioButton rb_raw;
        private System.Windows.Forms.TextBox txt_eingabe;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.TextBox txt_output_file_path;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_run;
        private System.Windows.Forms.RadioButton rb_hex;
        private System.Windows.Forms.RadioButton rb_hex_output;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btn_open_run;
        private System.Windows.Forms.Button btn_save_run;
        private System.Windows.Forms.GroupBox group_save;
        private System.Windows.Forms.RadioButton rb_sr_digi;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton rb_sr_8bit;
        private System.Windows.Forms.GroupBox group_open;
        private System.Windows.Forms.RadioButton rb_open_text;
        private System.Windows.Forms.RadioButton rb_open_raw;
        private System.Windows.Forms.NumericUpDown num_samplerate;
        private System.Windows.Forms.RadioButton rb_uart_lsb;
    }
}

