﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace raw_transmissiondata_converter
{
    class Bit_manipulation
    {
        private byte[] array;
        private int position = 0;

        public Bit_manipulation(byte[] bytes)
        {
            array = bytes;
            position = 0;
        }

        public void Set_position(int pos = 0) { position = pos; }
        public void Change_position(int pos = 0) { position += pos; }
        public bool Get_Bit_bool()
        {
            return Get_Bit_byte() != 0;
        }
        public bool Get_Bit_bool_inc()
        {
            bool tmp = Get_Bit_byte() != 0;
            position++;
            return tmp;
        }
        public byte Get_Bit_byte_inc()
        {
            byte tmp = Get_Bit_byte();
            position++;
            return tmp;
        }
        public bool Get_Bit_bool_dec(bool readfirst = true)
        {
            bool tmp;
            if (readfirst)
            {
                tmp = Get_Bit_byte() != 0;
                position--;
                if (position < 0)
                    position = 0;
            }
            else
            {
                position--;
                if (position < 0)
                    position = 0;
                tmp = Get_Bit_byte() != 0;
            }
            return tmp;
        }
        public byte Get_Bit_byte_dec(bool readfirst = true)
        {
            byte tmp;
            if (readfirst)
            {
                tmp = Get_Bit_byte();
                position--;
                if (position < 0)
                    position = 0;
            }
            else
            {
                position--;
                if (position < 0)
                    position = 0;
                tmp = Get_Bit_byte();
            }            
            return tmp;
        }
        public byte Get_Bit_byte()
        {
            return (byte)(array[position / 8] & (1 << (7 - (position % 8))));
        }
        public bool Get_last_Bit_bool(bool default_val = false)
        {
            position--;
            if (position < 0) {
                position = 0;
                return default_val;
             }
            return Get_Bit_byte_inc() != 0;
        }
        public byte Get_last_Bit_byte(byte default_val = 0)
        {
            position--;
            if (position < 0) return default_val;
            return Get_Bit_byte_inc();
        }
        public void Set_Bit(bool bit)
        {
            if (bit)
                array[position / 8] |= (byte)(1 << (7 - (position % 8)));
            else
                array[position / 8] &= (byte)(~(1 << (7 - (position % 8))));
        }
        public void Set_Bit(byte bit)
        {
            Set_Bit(bit != 0);
        }
        public void Set_Bit_inc(bool bit)
        {
            Set_Bit(bit);
            position++;
        }
        public void Set_Bit_inc(byte bit)
        {
            Set_Bit(bit != 0);
            position++;
        }
        public void Fill(bool bit)
        {
            for(int x = position; x < array.Length * 8; x++)
            {
                Set_Bit_inc(bit);
            }
        }
        public void Fill(byte bit)
        {
            Fill(bit != 0);
        }
        public void Fill_All(bool bit)
        {
            position = 0;
            Fill(bit);
        }
        public void Fill_All(byte bit)
        {
            Fill_All(bit != 0);
        }
    }
}
