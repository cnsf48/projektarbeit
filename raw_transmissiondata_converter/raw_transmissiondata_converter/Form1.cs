﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;

namespace raw_transmissiondata_converter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void btn_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.txt|*.txt|*|*";
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                txt_input_file_path.Text = ofd.FileName;
            }
        }
        private void btn_run_Click(object sender, EventArgs e)
        {
            byte[] bytes = null;
            byte[] output = null;
            var input_rb = group_input.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);
            switch (input_rb.Text)
            {
                case "RAW":
                    break;
                case "ASCII":
                    break;
                case "WAV":
                    bytes = EightBitAudio(txt_input_file_path.Text);
                    break;
                case "Texteingabe":
                    bytes = Encoding.ASCII.GetBytes(txt_eingabe.Text);
                    break;
                case "HEX":
                    bytes = HexStringToByteArray(txt_eingabe.Text);
                    break;
                default:
                    break;
            }
            var code_rb = group_code.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);
            switch (code_rb.Text)
            {
                case "UART":
                    output = Run_conversion_uart(bytes);
                    break;
                case "LSB":
                    output = Run_conversion_uart(bytes, true);
                    break;
                case "SPDIF":
                    output = Run_encode_spdif(bytes);
                    break;
                case "Morse":
                    break;
                case "Biphase Mark":
                    output = Run_encode_biphase_mark(bytes);
                    break;
                case "HEX":
                    output = bytes;
                    break;
                default:
                    break;
            }
            txt_output.Text = ByteArrayToString(output);
        }
        byte[] Run_conversion_uart(byte[] input, bool lsb = false)
        {
            double size = input.Length;
            double new_size = size * 10 / 8;
            new_size = Math.Ceiling(new_size) + 1;
            byte[] output;
            output = new byte[(int)new_size];
            Bit_manipulation output_array;
            output_array = new Bit_manipulation(output);
            Bit_manipulation input_array;
            input_array = new Bit_manipulation(input);
            output_array.Set_Bit_inc(true);
            output_array.Set_Bit_inc(true);
            output_array.Set_Bit_inc(true);
            output_array.Set_Bit_inc(true);
            for (int x = 0; x < size; x++)
            {
                output_array.Set_Bit_inc(false);
                if (lsb)
                {
                    input_array.Change_position(8);
                    for (int bit = 0; bit < 8; bit++)
                    {
                        output_array.Set_Bit_inc(input_array.Get_Bit_bool_dec(false));
                    }
                    input_array.Change_position(8);
                }
                else
                {
                    for (int bit = 0; bit < 8; bit++)
                    {
                        output_array.Set_Bit_inc(input_array.Get_Bit_bool_inc());
                    }
                }
                
                output_array.Set_Bit_inc(true);
            }
            output_array.Fill(true);
            return output;
        }
        void Append_Preable(Bit_manipulation data, char preamble, bool startbit = false)
        {
            bool tmp = data.Get_last_Bit_bool(startbit);
            switch (preamble)
            {
                case 'B':
                case 'b':
                    if (tmp)
                    {
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                    }
                    else
                    {
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                    }
                    break;
                case 'M':
                case 'm':
                    if (tmp)
                    {
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                    }
                    else
                    {
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                    }
                    break;
                case 'W':
                case 'w':
                    if (tmp)
                    {
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                    }
                    else
                    {
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(1);
                        data.Set_Bit_inc(0);
                        data.Set_Bit_inc(0);
                    }
                    break;
            }
        }
        void Run_encode_biphase_mark(byte[] input, Bit_manipulation output, bool startbit = false)
        {
            double size = input.Length;
            Bit_manipulation input_array;
            input_array = new Bit_manipulation(input);

            for(int x = 0; x < size * 8; x++)
            {
                bool last_bit = output.Get_last_Bit_bool(startbit);
                output.Set_Bit_inc(!last_bit);
                if (input_array.Get_Bit_bool_inc())
                    output.Set_Bit_inc(last_bit);
                else
                    output.Set_Bit_inc(!last_bit);
            }
        }
        void Run_encode_biphase_mark(bool input, Bit_manipulation output, bool startbit = false)
        {
                bool last_bit = output.Get_last_Bit_bool(startbit);
                output.Set_Bit_inc(!last_bit);
                if (input)
                    output.Set_Bit_inc(last_bit);
                else
                    output.Set_Bit_inc(!last_bit);

        }
        byte[] Run_encode_biphase_mark(byte[] input, bool startbit = false)
        {
            double size = input.Length;
            double new_size = size * 2;
            new_size = Math.Ceiling(new_size);
            byte[] output;
            output = new byte[(int)new_size];
            Bit_manipulation output_array;
            output_array = new Bit_manipulation(output);
            Bit_manipulation input_array;
            input_array = new Bit_manipulation(input);

            for (int x = 0; x < size * 8; x++)
            {
                bool tmp = output_array.Get_last_Bit_bool(startbit);
                output_array.Set_Bit_inc(!tmp);
                if (input_array.Get_Bit_bool_inc())
                    output_array.Set_Bit_inc(tmp);
                else
                    output_array.Set_Bit_inc(!tmp);
            }

            return output;
        }
        byte[] Run_encode_spdif(byte[] input)
        {
            int new_size = (int)(input.Length / 6);
            int[] left;
            int[] right;
            left = new int[new_size];
            right = new int[new_size];
            byte[] bytes;
            bytes = new byte[4];
            for (int x = 0; x < new_size; x++)
            {
                Array.Copy(input, x * 6, bytes, 1, 3);
                Array.Reverse(bytes);
                left[x] = BitConverter.ToInt32(bytes, 0);
                Array.Copy(input, x * 6 + 3, bytes, 1, 3);
                Array.Reverse(bytes);
                right[x] = BitConverter.ToInt32(bytes, 0);
            }
            return Run_encode_spdif(left, right);
        }
        byte[] Run_encode_spdif(Int32[] left, Int32[] right)
        {
            int samples = left.Length;
            int blocks = samples / 192;
            if (samples % 192 != 0) blocks++;
            int new_size = blocks * 192 * 16;
            if(left.Length < blocks * 192)
            {
                int[] tmp;
                tmp = new int[blocks * 192];
                left.CopyTo(tmp, 0);
                left = tmp;
            }
            if (right.Length < blocks * 192)
            {
                int[] tmp;
                tmp = new int[blocks * 192];
                right.CopyTo(tmp, 0);
                right = tmp;
            }
            byte[] output;
            output = new byte[new_size];
            Bit_manipulation output_array;
            output_array = new Bit_manipulation(output);
            bool last_bit = false;
            bool validity_bit = false; //Sample OK
            bool sub_code_data_bit = false;
            bool parity = false;
            bool[] audio_bits;
            bool[] channel_status;
            channel_status = new bool[192];
            for (int block = 0; block < blocks; block++)
            {
                for (int frame = 0; frame < 192; frame++)
                {
                    last_bit = output_array.Get_last_Bit_bool();
                    if (frame == 0) Append_Preable(output_array, 'b', last_bit);
                    else Append_Preable(output_array, 'm', last_bit);
                    audio_bits = Int_to_Bit(left[frame + block * 192]);
                    for (int x = 0; x < 24; x++)
                    {
                        parity ^= audio_bits[x];
                        Run_encode_biphase_mark(audio_bits[x], output_array);
                    }
                    validity_bit = false; //Sample OK
                    parity ^= validity_bit;
                    Run_encode_biphase_mark(validity_bit, output_array);
                    sub_code_data_bit = false; //Not used
                    parity ^= sub_code_data_bit;
                    Run_encode_biphase_mark(sub_code_data_bit, output_array);
                    parity ^= channel_status[frame];
                    Run_encode_biphase_mark(channel_status[frame], output_array);
                    Run_encode_biphase_mark(parity, output_array);

                    last_bit = output_array.Get_last_Bit_bool();
                    Append_Preable(output_array, 'w', last_bit);
                    audio_bits = Int_to_Bit(right[frame + block * 192]);
                    for (int x = 0; x < 24; x++)
                    {
                        parity ^= audio_bits[x];
                        Run_encode_biphase_mark(audio_bits[x], output_array);
                    }
                    validity_bit = false; //Sample OK
                    parity ^= validity_bit;
                    Run_encode_biphase_mark(validity_bit, output_array);
                    sub_code_data_bit = false; //Not used
                    parity ^= sub_code_data_bit;
                    Run_encode_biphase_mark(sub_code_data_bit, output_array);
                    parity ^= channel_status[frame];
                    Run_encode_biphase_mark(channel_status[frame], output_array);
                    Run_encode_biphase_mark(parity, output_array);
                }
            }

            return output;
        }
        bool[] Int_to_Bit(int x, byte bit = 24, bool reverse = false)
        {
            BitArray b = new BitArray(new int[] { x });
            bool[] temp = new bool[b.Length];
            bool[] bits = new bool[bit];
            b.CopyTo(temp, 0);
            Array.Copy(temp, 0, bits, 0, bit);
            if (reverse)
                Array.Reverse(bits);
            return bits;
            
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                //hex.AppendFormat("{0:x2}", b);
                hex.AppendFormat("0x{0:x2}, ", b);
            return hex.ToString();
        }
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        public static bool GetBit_bool(byte b, int bitNumber)
        {
            return (b & (1 << bitNumber)) != 0;
        }
        public static byte GetBit(byte b, int bitNumber)
        {
            return (byte)(b & (1 << bitNumber));
        }
        public static byte[] HexStringToByteArray(string hex)
        {
            //return Encoding.ASCII.GetBytes(hex);
            hex = hex.Replace("0x", "");
            hex = hex.Replace(",", "");
            hex = hex.Replace(" ", "");
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
        // convert two bytes to one double in the range -1 to 1
        static double bytesToDouble(byte firstByte, byte secondByte)
        {
            // convert two bytes to one short (little endian)
            short s = (short)((secondByte << 8) | firstByte);
            // convert to range from -1 to (just below) 1
            return s / 32768.0;
        }
        // Returns left and right double arrays. 'right' will be null if sound is mono.
        public void openWav(string filename, out double[] left, out double[] right)
        {
            int channels;
            openWav(filename, out left, out right, out channels);
        }
        public void openWav(string filename, out double[] left, out double[] right, out int channels)
        {
            byte[] wav = File.ReadAllBytes(filename);

            // Determine if mono or stereo
            channels = wav[22];     // Forget byte 23 as 99.999% of WAVs are 1 or 2 channels

            // Get past all the other sub chunks to get to the data subchunk:
            int pos = 12;   // First Subchunk ID from 12 to 16

            // Keep iterating until we find the data chunk (i.e. 64 61 74 61 ...... (i.e. 100 97 116 97 in decimal))
            while (!(wav[pos] == 100 && wav[pos + 1] == 97 && wav[pos + 2] == 116 && wav[pos + 3] == 97))
            {
                pos += 4;
                int chunkSize = wav[pos] + wav[pos + 1] * 256 + wav[pos + 2] * 65536 + wav[pos + 3] * 16777216;
                pos += 4 + chunkSize;
            }
            pos += 8;

            // Pos is now positioned to start of actual sound data.
            int samples = (wav.Length - pos) / 2;     // 2 bytes per sample (16 bit sound mono)
            if (channels == 2) samples /= 2;        // 4 bytes per sample (16 bit stereo)

            // Allocate memory (right will be null if only mono sound)
            left = new double[samples];
            if (channels == 2) right = new double[samples];
            else right = null;

            // Write to double array/s:
            int i = 0;
            while (pos < wav.Length)
            {
                left[i] = bytesToDouble(wav[pos], wav[pos + 1]);
                pos += 2;
                if (channels == 2)
                {
                    right[i] = bytesToDouble(wav[pos], wav[pos + 1]);
                    pos += 2;
                }
                i++;
            }
        }
        byte[] EightBitAudio(string filename)
        {
            double[] left;
            double[] right;
            int channels;
            openWav(filename, out left, out right, out channels);
            byte[] mono;
            mono = new byte[left.Length];
            if (channels > 1)
            { //Stereo
                for(int x=0; x < left.Length; x++)
                {
                    mono[x] = (byte)((left[x] + right[x] + 2) * 255 / 4);
                }
            }
            else
            { //Mono
                for (int x = 0; x < left.Length; x++)
                {
                    mono[x] = (byte)((left[x] + 1) * 255 / 2);
                }
            }
            return mono;
        }
        private void txt_input_file_path_TextChanged(object sender, EventArgs e)
        {
            if (txt_input_file_path.Text.Length < 1) return;
            btn_open_run.Enabled = File.Exists(txt_input_file_path.Text);           
        }
        private void txt_output_file_path_TextChanged(object sender, EventArgs e)
        {
            if (txt_output_file_path.Text.Length < 1) return;
            bool dir = Directory.Exists(Path.GetDirectoryName(txt_output_file_path.Text));
            bool ext;
            if (Path.GetExtension(txt_output_file_path.Text) != String.Empty)
                ext = true;
            else
                ext = false;
            btn_save_run.Enabled = dir && ext;
        }
        private void btn_save_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "*.txt|*.txt|*.sr|*.sr|*|*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                txt_output_file_path.Text = sfd.FileName;
            }
        }
        private void btn_open_run_Click(object sender, EventArgs e)
        {
            var open_rb = group_open.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);
            switch (open_rb.Text)
            {
                case "RAW":
                    if (File.Exists(txt_input_file_path.Text))
                    {
                        FileStream fileStream = new FileStream(txt_input_file_path.Text, FileMode.Open);
                        byte[] bytes = new byte[fileStream.Length];
                        for (int i = 0; i < fileStream.Length; i++)
                        {
                            bytes[i] = (byte)fileStream.ReadByte();
                        }
                        txt_eingabe.Text = ByteArrayToString(bytes);
                    }                    
                    break;
                case "Text":
                    if (File.Exists(txt_input_file_path.Text))
                    {
                        txt_eingabe.Text = File.ReadAllText(txt_input_file_path.Text);
                    }
                    break;
                default:
                    break;
            }
        }
        private void btn_save_run_Click(object sender, EventArgs e)
        {
            byte[] bytes;                    
            var save_rb = group_save.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);
            switch (save_rb.Text)
            {
                case "RAW":
                    FileStream fileStream = new FileStream(txt_output_file_path.Text, FileMode.Create);
                    bytes = HexStringToByteArray(txt_output.Text);
                    for (int i = 0; i < bytes.Length; i++)
                    {
                        fileStream.WriteByte(bytes[i]);
                    }
                    fileStream.Close();
                    break;
                case "Text":
                    File.WriteAllText(txt_output_file_path.Text, txt_output.Text);
                    break;
                case "Sigrok Digital":
                    bytes = HexStringToByteArray(txt_output.Text);
                    save_sr(bytes, txt_output_file_path.Text, "digital", (double)num_samplerate.Value);
                    break;
                case "Sigrok 8 Bit":
                    bytes = HexStringToByteArray(txt_output.Text);
                    save_sr(bytes, txt_output_file_path.Text, "analog", (double)num_samplerate.Value);
                    break;
                default:
                    break;
            }
        }
        void save_sr(byte[] bytes, string filename, string typ = "digital", double frequency = 5000000)
        {
            if (File.Exists(filename))
                File.Delete(filename);
            ZipArchive zipArchive = ZipFile.Open(filename, ZipArchiveMode.Create);
            string samplerate = Convert.ToInt32(frequency).ToString() + " Hz";
            ZipArchiveEntry archiveEntry;
            StreamWriter streamWriter; // generate some metadata
            archiveEntry = zipArchive.CreateEntry("version");
            streamWriter = new StreamWriter(archiveEntry.Open());
            streamWriter.WriteLine("2");
            streamWriter.Close();
            streamWriter.Dispose();
            archiveEntry = zipArchive.CreateEntry("metadata");
            streamWriter = new StreamWriter(archiveEntry.Open());
            streamWriter.WriteLine("[global]");
            streamWriter.WriteLine("[device 1]");
            streamWriter.WriteLine("samplerate = " + samplerate);
            switch (typ)
            {
                case "digital":
                    streamWriter.WriteLine("capturefile=logic-1");
                    streamWriter.WriteLine("total probes = 1");
                    streamWriter.WriteLine("probe1 = D1");
                    break;
                case "analog":
                    streamWriter.WriteLine("total analog = 1");
                    streamWriter.WriteLine("analog1 = A1");
                    break;
                default:
                    break;
            }
            streamWriter.WriteLine("unitsize=1");
            streamWriter.Close();
            streamWriter.Dispose();
            Stream stream_analog; // save compressed streams
            switch (typ)
            {
                case "digital":
                    archiveEntry = zipArchive.CreateEntry("logic-1-1");
                    stream_analog = archiveEntry.Open();
                    byte[] digital_output;
                    digital_output = new byte[bytes.Length * 8];
                    Bit_manipulation bytes_bit = new Bit_manipulation(bytes);
                    for(int x = 0; x < digital_output.Length; x++){
                        if(bytes_bit.Get_Bit_bool_inc())
                            digital_output[x] = 0xff;
                        else
                            digital_output[x] = 0x00;
                    }
                    stream_analog.Write(digital_output, 0, digital_output.Length);
                    stream_analog.Close();
                    break;
                case "analog":
                    archiveEntry = zipArchive.CreateEntry("analog-1-1-1");
                    stream_analog = archiveEntry.Open();
                    byte[] analog_output;
                    analog_output = new byte[bytes.Length *4];
                    int index = 0;
                    float value;
                    byte[] b;
                    for (int i = 0; i < bytes.Length; i++)
                    { // convert sample by sample to float and store values in byte array (easy to save)
                         {
                            value = Convert.ToSingle(bytes[i]);
                            b = BitConverter.GetBytes(value);
                            analog_output[index + 0] = b[0];
                            analog_output[index + 1] = b[1];
                            analog_output[index + 2] = b[2];
                            analog_output[index + 3] = b[3];
                        }
                        index += 4;
                    }
                    stream_analog.Write(analog_output, 0, analog_output.Length);
                    stream_analog.Close();
                    break;
                default:
                    break;
            }            
            zipArchive.Dispose();            
        }
    }
}
