#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <wiringPiSPI.h>
#include <pigpio.h>
#include "print.hpp"
#include "lcd.hpp"
#include "menu.hpp"

#define StringLenght 100
#define speed 32000000
#define i2c_adress 0x3c
#define spi_channel 0
	

#define bits 16
#define base_frequency 133000000

void spi_send_data(unsigned char* buffer, size_t size){
	unsigned char* internal_buffer = (unsigned char*) malloc(size);
	memcpy (internal_buffer, buffer, size);
	wiringPiSPIDataRW(spi_channel, internal_buffer, size) ;
	free (internal_buffer);
}

unsigned char get_keys(int device){
	static unsigned char last_value = 0;
	unsigned char value = wiringPiI2CReadReg8 (device, 0x05);
	unsigned char return_value = last_value & ~value;
	//if(return_value || value & 2 || last_value & 2){
	//	printf("Keys: 0x%02x\n", return_value);
	//	printf("Keys now: 0x%02x\n", value);
	//	printf("Keys last: 0x%02x\n", last_value);		
	//}
	last_value = value;
	return return_value;
}

int counter_diff(int old_value, int new_value, int bit = 8){
	static int bit_buffer = 0;
	static int bits_pow = 0;
	static int bits_pow_2 = 0;
	if(bit_buffer != bits){
		bits_pow = pow(2,bit);
		bits_pow_2 = pow(2,bit-1);
	}
	int diff = new_value - old_value;
	if(diff)
		//printf("encoder old %i new %i\n", old_value, new_value);
	if(diff < bits_pow_2){
		if(-diff > bits_pow_2)		
			return diff + bits_pow;
		else
			return diff;
	}	 
	else
		return diff - bits_pow;
}

int get_encoder(int device){
	static bool first_run = 1;
	int new_val = 0;
	static int old_val = 0;
	new_val = wiringPiI2CReadReg8 (device, 0x04) ;
	if(first_run){
		old_val = new_val;
		first_run = 0;
	}
	int i = counter_diff(old_val, new_val);
	old_val = new_val;
	return i;
}

double set_frequency(int device, double frequency){
	unsigned char group = 0;
	unsigned int count = 0;
	double new_base_frequency = base_frequency;
	double min_group_frequency = base_frequency;
	int x = 0;	
	while(min_group_frequency > frequency){
		min_group_frequency = new_base_frequency / (pow(2, bits)-1);
		x++;
		new_base_frequency = base_frequency / pow(2, x);
	}
	x--;
	if(x<0){
		printf("frequency out of range\n");
		return -1;
	}
	group = x;
	new_base_frequency = base_frequency / pow(2, x);
	min_group_frequency = new_base_frequency / (pow(2, bits));
	double last_value = min_group_frequency;
	double last_difference = abs(frequency - last_value);
	double value;
	double current_difference;
	for( x = pow(2, bits) ; x > 0; x--){
		value = new_base_frequency / x;
		current_difference = abs(frequency - value);
		if(last_difference < current_difference) 
			break;
		last_difference = current_difference;
	}
	count = x + 1;
	wiringPiI2CWriteReg8 (device, 0x02, group);
	wiringPiI2CWriteReg8 (device, 0x03, (unsigned char)(x & 0xFF));
	wiringPiI2CWriteReg8 (device, 0x01, (unsigned char)((x >> 8) & 0xFF));
	return (base_frequency / pow(2, group)) / count;
}

int main(void){
	wiringPiSetupSys();
	if (gpioInitialise() < 0){
		printf("Error while Initialise pigpio\n");
		return 1;
	} 

	int fpga_i2c;
	fpga_i2c = wiringPiI2CSetup(i2c_adress);
	lcd_device_nr = fpga_i2c;
	int fpga_spi;
	fpga_spi = wiringPiSPISetup (spi_channel, speed) ;

	//char mystring [StringLenght];

	if(fpga_i2c == -1){
		printf("No I2C device found\n");
		return 0;
	}
	if(fpga_spi == -1){
		printf("Cant open SPI device\n");
		return 0;
	}

	lcd_init();
	engine = new Menu::Engine(&Menu::NullItem);
	menuExit(Menu::actionDisplay); // reset to initial state


	unsigned char keys = 0;


	char * test_text = "Append Test";
    engine->append_Item (&miTest9, test_text, (Menu::Callback_t)&menuDummy, default_type, NULL, NULL) ;


	while (true)
	{
		keys = get_keys(fpga_i2c);
		lcd_set_cursor(0,0);
		if(keys & 1){ //Extra Button
			if (systemState == State::Settings) {
				printf("Extra Button Navigate\n");
				engine->navigate(engine->getParent());
				updateMenu = true;
			}else if(systemState == State::Edit){
				if(last_type != NULL)
				switch(last_type){
					case v_u_char:
					case v_s_char:
						//(*last_value).value -= 10;
						break;
					case v_bool:
					case v_team:
						//(*last_value).value = 0;
						break;
					case v_u_int:
						(*(unsigned int*)last_value) = 0;
						break;
					case v_s_int:
						(*(int*)last_value) = 0;
						break;
				}
				updateMenu = true;
			}
		}
		if(keys & 2){ //Encoder Pressed
			if (systemState == State::Edit) { 
				systemState = State::Settings;
				previousSystemState = systemState;
				updateMenu = true;
				
			}else if (systemState == State::Settings) {
				engine->invoke();
				updateMenu = true;
			}else{// enter settings menu
				engine->navigate(&miSettings);
				systemState = State::Settings;
				previousSystemState = systemState;
				updateMenu = true;
			}
		}
		int i = get_encoder(fpga_i2c);
		if(i>0){ //UP
			if (systemState == State::Settings) {
				engine->navigate(engine->getNext());
				updateMenu = true;
			}else if(systemState == State::Edit){
				if(last_type != NULL)
				switch(last_type){
					case v_u_char:
						(*(uint8_t*)last_value) += i;
						break;
					case v_s_char:
						(*(int8_t*)last_value) += i;
						break;
					case v_bool:
					case v_team:
						(*(bool*)last_value) = !(*(bool*)last_value);
						break;
					case v_u_int:
						(*(unsigned int*)last_value) += i;
						break;
					case v_s_int:
						(*(int*)last_value) += i;
						break;
						
				}
				updateMenu = true;
			}
		}
		if(i<0){ //DOWN
			if (systemState == State::Settings) {
				engine->navigate(engine->getPrev());
				updateMenu = true;
			}else if(systemState == State::Edit){
				if(last_type != NULL)
				switch(last_type){
					case v_u_char:
						(*(uint8_t*)last_value) += i;
						break;
					case v_s_char:
						(*(int8_t*)last_value) += i;
						break;
					case v_bool:
					case v_team:
						(*(bool*)last_value) = !(*(bool*)last_value);
						break;
					case v_u_int:
						(*(unsigned int*)last_value) += i;
						break;
					case v_s_int:
						(*(int*)last_value) += i;
						break;
				}
				updateMenu = true;
			}
		}
		if (updateMenu) {
			updateMenu = false;
			lcd_clear();
			// render the menu
			if (systemState == State::Settings){
				//ScopedTimer tm("render menu");
				engine->render(renderMenuItem, menuItemsVisible);
			}
			if (systemState == State::Edit){
				//ScopedTimer tm("render menu");
				engine->render(renderMenuItem, menuItemsVisible);
			}
			lcd_update();
		}
	}
	return 0;
}