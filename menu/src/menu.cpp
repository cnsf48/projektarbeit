#if  1
// ----------------------------------------------------------------------------
// MicroMenu
// Copyright (c) 2014 karl@pitrich.com
// All rights reserved.
// License: MIT
// ----------------------------------------------------------------------------

#include "menu.hpp"
#include "lcd.hpp"
#include "print.hpp"

uint8_t speed_update = 1;
bool updateMenu = false;
var_type last_type;
void * last_value;
char null_text[] = "Null Item!!!";
// ----------------------------------------------------------------------------

namespace Menu {

	// ----------------------------------------------------------------------------

	Item_t NullItem = { (Menu::Item_s *)NULL, (Menu::Item_s *)NULL, (Menu::Item_s *)NULL, (Menu::Item_s *)NULL, NULL, (var_type)NULL, NULL, (char *)null_text };

	// ----------------------------------------------------------------------------

	Engine::Engine()
	: currentItem((Item_t *)&Menu::NullItem), previousItem((Item_t *)&Menu::NullItem), lastInvokedItem((Item_t *)&Menu::NullItem)
	{}

	Engine::Engine(Item_t *initialItem)
	: currentItem((Item_t *)initialItem), previousItem((Item_t *)&Menu::NullItem), lastInvokedItem((Item_t *)&Menu::NullItem)
	{}
	
	// ----------------------------------------------------------------------------

	char * Engine::getLabel(Item_t * item) {
		return (char *)((item == NULL) ? currentItem->Label : item->Label);
	}
	var_type Engine::getType(Item_t * item) {
		return (var_type)((item == NULL) ? currentItem->Value_type : item->Value_type);
	}
	var_type Engine::getType() {
		return (var_type)currentItem->Value_type;
	}	
	void * Engine::getValue(Item_t * item) {
		return (void *)((item == NULL) ? currentItem->Value : item->Value);
	}
	void * Engine::getValue() {
		return (void *)currentItem->Value;
	}	
	void Engine::setDefaultValue() {
		switch(currentItem->Value_type){
			case v_u_char:
			case v_u_char_ro:
				*(uint8_t *)(currentItem->Value) = *(uint8_t *)(currentItem->Value_default);
				break;
			case v_s_char:
			case v_s_char_ro:
				*(int8_t *)(currentItem->Value) = *(int8_t *)(currentItem->Value_default);
				break;
			case v_bool:
			case v_bool_ro: //read-only
			case v_team:
				*(bool *)(currentItem->Value) = *(bool *)(currentItem->Value_default);
				break;
			case v_u_int:
				*(unsigned int *)(currentItem->Value) = *(unsigned int *)(currentItem->Value_default);
				break;
			case v_s_int:
				*(int *)(currentItem->Value) = *(int *)(currentItem->Value_default);
				break;
		}
	}	
	Item_t * Engine::getPrev(Item_t * item) {
		return (Item_t *)((item == NULL) ? currentItem->Previous : item->Previous);
	}
	Item_t * Engine::getNext(Item_t * item) {
		return (Item_t *)((item == NULL) ? currentItem->Next : item->Next);
	}
	Item_t * Engine::getParent(Item_t * item) {
		return (Item_t *)((item == NULL) ? currentItem->Parent : item->Parent);
	}
	Item_t * Engine::getChild(Item_t * item) {
		return (Item_t *)((item == NULL) ? currentItem->Child : item->Child);
	}

	void Engine::append_Item (Item_t * item, char * Label) {		
		append_Item(item, Label, &menuDummy, default_type, NULL, NULL);
	}
	void Engine::append_Item (Item_t * item, char * Label, Callback_t Callback) {
		append_Item(item, Label, Callback, default_type, NULL, NULL);
	}
	void Engine::append_Item (Item_t * item, char * Label, Callback_t Callback, var_type Value_type, void* Value) {
		append_Item(item, Label, Callback, Value_type, Value, NULL);
	}
	void Engine::append_Item (Item_t * item, char * Label, Callback_t Callback, var_type Value_type, void* Value, void* Value_default) {
		Item_t * new_Item = new Menu::Item_t();
		new_Item->Next = item->Next;
		new_Item->Previous = item;
		item->Next = new_Item;
		new_Item->Parent = item->Parent;
		new_Item->Child = item->Child;
		new_Item->Callback = Callback;
		new_Item->Value_type = Value_type;
		new_Item->Value = Value;
		new_Item->Value_default = Value_default;
		new_Item->Label = Label;
	}
	// ----------------------------------------------------------------------------

	void Engine::navigate(Item_t * targetItem) {
		uint8_t commit = true;
		if (targetItem && targetItem != &Menu::NullItem) {
			if (targetItem == getParent(currentItem)) { // navigating back to parent
				commit = executeCallbackAction(actionParent); // exit/save callback
				lastInvokedItem = (Item_t *)&Menu::NullItem;
			}
			if (commit) {
				previousItem = (Item_t *)currentItem;
				currentItem = (Item_t *)targetItem;
				executeCallbackAction(actionLabel);
			}
		}
	}

	// ----------------------------------------------------------------------------

	void Engine::invoke(void) {
		bool preventTrigger = false;

		//if (lastInvokedItem != currentItem) { // prevent 'invoke' twice in a row
			//lastInvokedItem = currentItem;
			//preventTrigger = true; // don't invoke 'trigger' at first Display event
			executeCallbackAction(actionDisplay);
		//}

		Item_t *child = getChild();
		if (child && child != &Menu::NullItem) { // navigate to registered submenuitem
			navigate(child);
		}
		else { // call trigger in already selected item that has no child
			if (!preventTrigger) {
				executeCallbackAction(actionTrigger);
			}
		}
	}

	// ----------------------------------------------------------------------------

	bool Engine::executeCallbackAction(Action_t action) {
		if (currentItem && currentItem != NULL) {
			Callback_t callback = (Callback_t)(currentItem->Callback);
			if (callback != NULL) {
				return (*callback)(action);
			}
		}
		return true;
	}

	// ----------------------------------------------------------------------------

	Info_t Engine::getItemInfo(Item_t * item) {
		Info_t result = { 0, 0 };
		Item_t * i = getChild(getParent());
		//for (; i && i != &Menu::NullItem && &i->Next && i->Next != &Menu::NullItem; i = getNext(i)) {
		for (; i && i != &Menu::NullItem && &i->Next; i = getNext(i)) {
			result.siblings++;
			if (i == item) {
				result.position = result.siblings;
				//printf("Debug result.position: %i\n", result.position);
			}
		}

		return result;
	}

	// ----------------------------------------------------------------------------

	void Engine::render(RenderCallback_t render, uint8_t maxDisplayedMenuItems) {
		if (!currentItem || currentItem == &Menu::NullItem) {
			return;
		}
		uint8_t start = 0;
		uint8_t itemCount = 0;
		const uint8_t center = maxDisplayedMenuItems >> 1;
		//printf("Debug center: %i\n", center);
		Info_t mi = getItemInfo(currentItem);
		
		//printf("Debug mi.position: %i\n", mi.position);
		if (mi.position >= (mi.siblings - center)) { // at end
			start = mi.siblings - maxDisplayedMenuItems;
			//printf("Debug at end\n");
		}
		else {
			start = mi.position - center;
			if (maxDisplayedMenuItems & 0x01) start--; // center if odd
			//printf("Debug center if odd\n");
		}
		//printf("Debug start1: %i\n", start);
		if (start & 0x80){
			start = 0; // prevent overflow
			//printf("Debug prevent overflow\n");

		}

		//printf("Debug start2: %i %i\n", start, mi.siblings);

		// first item in current menu level
		Item_t * i = getChild(getParent());
		//for (; i && i != &Menu::NullItem && &i->Next && i->Next != &Menu::NullItem; i = getNext(i)) {
		for (; i && i != &Menu::NullItem && &i->Next; i = getNext(i)) {
			if (itemCount - start >= maxDisplayedMenuItems) break;
			if (itemCount >= start) render(i, itemCount - start);
			itemCount++;
		}
	}

	// ----------------------------------------------------------------------------

}; // end namespace

// ----------------------------------------------------------------------------

Menu::Engine *engine;
Menu::Item_t *active_menu_item;

void * operator new(size_t size){
	return malloc(size);
}

void operator delete(void * ptr){
	free(ptr);
}

// ----------------------------------------------------------------------------

uint8_t systemState = State::Default;
uint8_t previousSystemState = State::None;

// ----------------------------------------------------------------------------

bool menuExit(Menu::Action_t a) {
	systemState = State::Default;
	lcd_clear();
	speed_update = 1;
	return true;
}
bool menuExit2(Menu::Action_t a) {
	if (a == Menu::actionDisplay) {
		systemState = State::Default;
		lcd_clear();
		speed_update = 1;
	}	
	return true;
}

bool menuDummy(Menu::Action_t a) {
	return true;
}

bool menuBack(Menu::Action_t a) {
	if (a == Menu::actionDisplay) {
		engine->navigate(engine->getParent(engine->getParent()));
	}
	return true;
}

bool menuEdit(Menu::Action_t a) {
	if (a == Menu::actionDisplay) {
		systemState = State::Edit;
		speed_update = 1;
	}
	return true;
}

bool menuDefault(Menu::Action_t a) {
	if (a == Menu::actionDisplay) {
		engine->setDefaultValue();
	}
	return true;
}

bool menuSave(Menu::Action_t a) {
	if (a == Menu::actionDisplay) {
		//save_ee();
	}
	return true;
}

bool menuShutdown(Menu::Action_t a) {
	if (a == Menu::actionDisplay) {
		if(enable_shutdown)
			system("sudo shutdown -h now");
	}
	return true;
}


// ----------------------------------------------------------------------------

uint8_t menuItemsVisible = rows;

void renderMenuItem(Menu::Item_t *menu_item, uint8_t pos) {
	//ScopedTimer tm("  render menuitem");

	//cursor_y = pos;
	//cursor_x = 0;
	//printf("Debug Pos: %i %s\n", pos, engine->getLabel(menu_item));
	lcd_set_cursor(0,pos);
	if(engine->currentItem == menu_item){
		active_menu_item = menu_item;
		lcd_printf(">");
	}
	
	lcd_set_cursor(1,pos);
	lcd_printf("%s",engine->getLabel(menu_item));
	// mark items that have children
	if (engine->getChild(menu_item) != &Menu::NullItem) {
		lcd_set_cursor(9,pos);
		lcd_printf("*");
	}else if (engine->getValue(menu_item) != NULL) {
		lcd_set_cursor(9,pos);
		if(systemState == State::Edit && engine->currentItem == menu_item){
			lcd_printf(">");
			last_type = engine->getType(menu_item);
			last_value = engine->getValue(menu_item);
			//printf("last_type %i\nlast_value %i\n", (int)last_type, *(int*)last_value);////////////////////////
		}else{
			lcd_printf(" ");
		}
		var_type Value_type = engine->getType(menu_item);
		//printf("renderMenuItem %i %i %s\n", pos, (int)Value_type, engine->getLabel(menu_item));////////////////////////
		if(Value_type != NULL)
		switch(Value_type){
			case v_u_char:
			case v_u_char_ro:
				//printf("renderMenuItem v_u_char %4u\n", *(uint8_t*)engine->getValue(menu_item));////////////////////////
				lcd_printf("%4u", *(uint8_t*)engine->getValue(menu_item));
				break;
			case v_s_char:
			case v_s_char_ro:
				int8_t num;
				num = *(int8_t*)engine->getValue(menu_item);
				if(num<128)
					lcd_printf("%4i", num);
				else
					lcd_printf("%4i", num-256);
				break;
			case v_bool:
			case v_bool_ro: //read-only
				if(*(bool*)engine->getValue(menu_item))
					lcd_printf("true");
				else
					lcd_printf("false");
				break;
			case v_team:
				//Show the team color
				if(*(bool*)engine->getValue(menu_item))
					lcd_printf("green");	
				else
					lcd_printf("orange");
				break;
			case v_u_int:
				lcd_printf("%6u", *(unsigned int*)engine->getValue(menu_item));
				break;
			case v_s_int:
				lcd_printf("%6i", *(int*)engine->getValue(menu_item));
				break;
		}
		
	}
}

const bool v_false = false;

// ----------------------------------------------------------------------------

//		 Name,			Label,			Next,			Previous,		Parent,			Child,			Callback,		Type,		Variable(Pointer)	Variablen_default(Pointer)
MenuItem(miExit2,		"exit",			Menu::NullItem,	Menu::NullItem,	Menu::NullItem,	miSettings,		menuExit,		NULL,		NULL,				NULL);
MenuItem(miExit,		"exit",			miExit2,		miExit2,		miExit2,		miSettings,		menuExit,		NULL,		NULL,				NULL);
//MenuItem(miExit,		"exit",			Menu::NullItem,	Menu::NullItem,	miExit2,		miSettings,		menuExit,		NULL,		NULL,				NULL);

MenuItem(miSettings,	"Settings",		miTest1,		Menu::NullItem,	miExit,			miCalibrateLo,	menuDummy,		NULL,		NULL,				NULL);

MenuItem(miCalibrateLo,	"Calibrate Lo",	miCalibrateHi,	Menu::NullItem,	miSettings,		Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);
MenuItem(miCalibrateHi,	"Calibrate Hi",	miChannel0,		miCalibrateLo,	miSettings,		Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);

MenuItem(miChannel0,	"Channel 0",	miTime,			miCalibrateHi,	miSettings,		miChView0,		menuDummy,		NULL,		NULL,				NULL);
MenuItem(miChView0,		"Ch0:View",		miChScale0,		Menu::NullItem,	miChannel0,		Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);
MenuItem(miChScale0,	"Ch0:Scale",	Menu::NullItem,	miChView0,		miChannel0,		Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);

MenuItem(miTime,		"Zeit",			Menu::NullItem,	miChannel0,		miSettings,		miStunden,		menuDummy,		NULL,		NULL,				NULL);
MenuItem(miStunden,		"Stunden",		miMinuten,		Menu::NullItem,	miTime,			Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);
MenuItem(miMinuten,		"Minuten",		miSekunden,		miStunden,		miTime,			Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);
MenuItem(miSekunden,	"Sekunden",		miChBack1,		miMinuten,		miTime,			Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);
MenuItem(miChBack1,		"Back",			Menu::NullItem,	miSekunden,		miTime,			Menu::NullItem,	menuBack,		NULL,		NULL,				NULL);

MenuItem(miTest1,		"Start",		miTest2,		miSettings,		miExit,			Menu::NullItem,	menuDummy,		NULL,		NULL,				NULL);
MenuItem(miTest2,		"Test Edit U8",	miTest3,		miTest1,		miExit,			Menu::NullItem,	menuEdit,		v_u_char,	&test1,				NULL);
MenuItem(miTest3,		"Test Edit S8",	miTest4,		miTest2,		miExit,			Menu::NullItem,	menuEdit,		v_s_char,	&test2,				NULL);
MenuItem(miTest4,		"Test EditUI",	miTest5,		miTest3,		miExit,			Menu::NullItem,	menuEdit,		v_u_int,	&test4,				NULL);
MenuItem(miTest5,		"Test EditSI",	miTestpunkte,	miTest4,		miExit,			Menu::NullItem,	menuEdit,		v_s_int,	&test5,				NULL);
MenuItem(miTestpunkte,	"Test Punkte",	miTestcounter,	miTest5,		miExit,			Menu::NullItem,	menuEdit,		NULL,		NULL,				NULL);
MenuItem(miTestcounter,	"Test Counter",	miTest6,		miTestpunkte,	miExit,			Menu::NullItem,	menuEdit,		v_bool,		&test3,				NULL);
MenuItem(miTest6,		"Backlight",	miTest7,		miTestcounter,	miExit,			Menu::NullItem,	menuDefault,	v_bool,		&test3,				&v_false);
MenuItem(miTest7,		"Team-color",	miShutdownen,	miTest6,		miExit,			Menu::NullItem,	menuEdit,		v_team,		&test3,				NULL);
MenuItem(miShutdownen,	"Shutdown en",	miShutdown,		miTest7,		miExit,			Menu::NullItem,	menuEdit,		v_bool,		&enable_shutdown,	NULL);
MenuItem(miShutdown,	"Shutdown   ",	miTest8,		miShutdownen,	miExit,			Menu::NullItem,	menuShutdown,	NULL,		NULL,				NULL);
MenuItem(miTest8,		"Exit",			miTest9,		miShutdown,		miExit,			Menu::NullItem,	menuExit2,		NULL,		NULL,				NULL);
MenuItem(miTest9,		"Save",			Menu::NullItem,	miTest8,		miExit,			Menu::NullItem,	menuSave,		NULL,		NULL,				NULL);


#endif