#include "lcd.hpp"

int lcd_device_nr;

char lcd_buffer[row_lenght * rows];
unsigned char lcd_cursor_x = 0;
unsigned char lcd_cursor_y = 0;

void lcd_init(){
	lcd_init(lcd_device_nr);
}
void lcd_init(int device){
	const unsigned char init_bytes [] {0x39,0x15,0x55,0x6E,0x70,0x38,0x0C,0x01,0x00};
	for(unsigned char x = 0; x < 9; x++)
		wiringPiI2CWriteReg8 (device, 0x11, init_bytes[x]);
	usleep(1000);
	wiringPiI2CWriteReg8 (device, 0x11, 0x06);
}

void lcd_puts(char* text){
	lcd_puts(lcd_device_nr, text);
}
void lcd_puts(int device, char* text){
	char c = *text;
	while(c){
		if (c != '\n')
			lcd_put (device, c);
		text++;
		c = *text;		
	}
}
void lcd_puts_direct(char* text){
	lcd_puts_direct(lcd_device_nr, text);
}
void lcd_puts_direct(int device, char* text){
	char c = *text;
	while(c){
		if (c != '\n')
			wiringPiI2CWriteReg8 (device, 0x10, c);
		text++;
		c = *text;		
	}
}

void lcd_put(char c){
	lcd_put(lcd_device_nr, c);
}
void lcd_put(int device, char c){
	if (c != '\n'){
		lcd_buffer[lcd_cursor_x + row_lenght * lcd_cursor_y] = c;
		lcd_cursor_x++;
		if(lcd_cursor_x >= row_lenght){
			lcd_cursor_x = 0;
			lcd_cursor_y++;
		}
		if(lcd_cursor_y >= rows)
			lcd_cursor_y = 0;
	}
}
void lcd_put_direct(char c){
	lcd_put_direct(lcd_device_nr, c);
}
void lcd_put_direct(int device, char c){
	if (c != '\n')
		wiringPiI2CWriteReg8 (device, 0x10, c);
}
void lcd_set_cursor(unsigned char x, unsigned char y){
	lcd_set_cursor(lcd_device_nr, x, y);
}
void lcd_set_cursor(int device, unsigned char x, unsigned char y){
	lcd_cursor_x = x;
	lcd_cursor_y = y;
	if(lcd_cursor_x >= row_lenght)
		lcd_cursor_x = 0;
	if(lcd_cursor_y >= rows)
		lcd_cursor_y = 0;
}
void lcd_set_cursor_direct(unsigned char x, unsigned char y){
	lcd_set_cursor_direct(lcd_device_nr, x, y);
}
void lcd_set_cursor_direct(int device, unsigned char x, unsigned char y){
	unsigned char pos = x + row_lenght * y;
	if(pos > rows*row_lenght)pos = 0;
	wiringPiI2CWriteReg8 (device, 0x11, 0x80|pos);
}

void lcd_clear(){
	lcd_clear(lcd_device_nr);
}
void lcd_clear(int device){
	lcd_set_cursor(device,0,0);
	for(int i = 0; i < (rows*row_lenght); i++)
	{
		lcd_put(device,' ');
	}
}

void lcd_update(){
	lcd_update(lcd_device_nr);
}
void lcd_update(int device){
	lcd_set_cursor_direct(0,0);
	for(int i = 0; i< row_lenght * rows; i++)
		lcd_put_direct(device, lcd_buffer[i]);
}

