#if  1
// ----------------------------------------------------------------------------
// MicroMenu
// Copyright (c) 2014 karl@pitrich.com
// All rights reserved.
// License: MIT
// ----------------------------------------------------------------------------

#ifndef __have_menu_h__
#define __have_menu_h__
//#ifdef  __cplusplus
//extern "C" {
//#endif

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>

#include "var.hpp"



extern uint8_t speed_update;
extern bool updateMenu;
extern var_type last_type;
extern void * last_value;

namespace Menu
{


  typedef enum Action_s {
    actionNone    = 0,
    actionLabel   = (1<<0), // render label when user scrolls through menu items
    actionDisplay = (1<<1), // display menu, after user selected a menu item
    actionTrigger = (1<<2), // trigger was pressed while menue was already active
    actionParent  = (1<<3), // before moving to parent, useful for e.g. "save y/n?" or autosave
    actionCustom  = (1<<7)  
  } Action_t;

  typedef bool (*Callback_t)(Action_t);

  typedef struct Info_s {
    uint8_t siblings;
    uint8_t position;
  } Info_t;

  typedef struct Item_s {
    struct Item_s * Next;
    struct Item_s * Previous;
    struct Item_s * Parent;
    struct Item_s * Child;
    Callback_t Callback;
    var_type Value_type;
    void * Value;
    void * Value_default;
    char * Label;  
  } Item_t;

  typedef void (*RenderCallback_t)(Item_t *, uint8_t);

  // a typesafe null item
  extern Item_t NullItem;

  class Engine {
  public:
    Item_t * currentItem;
    Item_t * previousItem;
    Item_t * lastInvokedItem; 

  public:
    Engine();
    Engine(Item_t * initialItem);

  public:
    void navigate(Item_t * targetItem);
    void invoke(void);
    bool executeCallbackAction(Action_t action);
    void render(RenderCallback_t render, uint8_t maxDisplayedMenuItems);

  public:
    Info_t getItemInfo(Item_t * item);
    char * getLabel (Item_t * item = NULL);
    var_type getType (Item_t * item);
    var_type getType ();
    void * getValue (Item_t * item);
    void * getValue ();
    void setDefaultValue();
    Item_t * getPrev  (Item_t * item = NULL);
    Item_t * getNext  (Item_t * item = NULL);
    Item_t * getParent(Item_t * item = NULL);
    Item_t * getChild (Item_t * item = NULL);
    //void append_Item (Item_t * item, char * Label, Callback_t Callback, var_type Value_type = default_type, void* Value = NULL, void* Value_default = NULL) ;
    void append_Item (Item_t * item, char * Label) ;
    void append_Item (Item_t * item, char * Label, Callback_t Callback) ;
    void append_Item (Item_t * item, char * Label, Callback_t Callback, var_type Value_type, void* Value) ;
    void append_Item (Item_t * item, char * Label, Callback_t Callback, var_type Value_type, void* Value, void* Value_default) ;
  };
}; // end namespace Menu

// ----------------------------------------------------------------------------

extern Menu::Engine *engine;
extern Menu::Item_t *active_menu_item;

#define MenuItem(Name, Label, Next, Previous, Parent, Child, Callback, Value_type, Value, Value_default) \
  extern Menu::Item_t Next, Previous, Parent, Child; \
  Menu::Item_t Name = { &Next, &Previous, &Parent, &Child, &Callback, (var_type)Value_type, (void*)Value, (void*)Value_default, Label }

extern Menu::Item_t miSettings;
extern Menu::Item_t miTest9;

#define clampValue(val, lo, hi) if (val > hi) val = hi; if (val < lo) val = lo;
#define maxValue(a, b) ((a > b) ? a : b)
#define minValue(a, b) ((a < b) ? a : b)

// ----------------------------------------------------------------------------
//#ifdef  __cplusplus
//}
//#endif

// ----------------------------------------------------------------------------

/*!
  template for callback handler

void menuCallback(menuAction_t action) {  
  if (action == Menu::actionDisplay) {
    // initialy entering this menu item
  }

  if (action == Menu::actionTrigger) {
    // click on already active item
  }

  if (action == Menu::actionLabel) {
    // show thy label but don't do anything yet
  }

  if (action == Menu::actionParent) { 
    // navigating to self->parent
  }
}
*/


void * operator new(size_t size);
void operator delete(void * ptr);

namespace State {
  typedef enum SystemMode_e {
    None      = 0,
    Default   = (1<<0),
    Settings  = (1<<1),
    Edit      = (1<<2)
  } SystemMode;
};

extern uint8_t systemState;
extern uint8_t previousSystemState;

bool menuExit(Menu::Action_t a);
bool menuExit2(Menu::Action_t a);
bool menuDummy(Menu::Action_t a);
bool menuBack(Menu::Action_t a);
bool menuEdit(Menu::Action_t a);
bool menuDefault(Menu::Action_t a);
bool menuSave(Menu::Action_t a);
bool menuShutdown(Menu::Action_t a);

extern uint8_t menuItemsVisible;
void renderMenuItem(Menu::Item_t *menu_item, uint8_t pos);


#endif // __have_menu_h__
#endif