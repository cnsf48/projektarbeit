#ifndef __Variables_h__
#define __Variables_h__

#include <stdio.h>
#include <inttypes.h>

enum var_type { default_type = 0, v_u_char, v_s_char, v_u_int, v_s_int, v_bool, v_team, v_u_char_ro, v_s_char_ro, v_bool_ro };

extern uint8_t test1;
extern int8_t test2;
extern bool test3;
extern unsigned int test4;
extern int test5;

extern bool enable_shutdown;

#endif